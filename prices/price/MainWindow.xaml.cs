﻿using System.Windows;
using System.Net.Http;
using HtmlAgilityPack;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace price
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        public class CarPartPriceAutodoc
        {
            private string _baseUrl = "https://www.autodoc.ru/web/price/art/";

            public double GetPrice(string article)
            {
                string url = _baseUrl + article.Replace(".", "");

                try
                {
                    var httpClient = new HttpClient();
                    string html = httpClient.GetStringAsync(url).Result;

                    var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                    htmlDoc.LoadHtml(html);

                    var bodyNode = htmlDoc.DocumentNode.SelectSingleNode("//table[@class='treeTable']/tr[1]/td[2]/span");
                    if (bodyNode != null)
                    {
                        double price = double.Parse(bodyNode.InnerText);
                        return price;
                    }
                }
                catch
                {
                    return 0;
                }

                return 0;
            }
        }
        
        public string GetPath()
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return dialog.FileName;
            }
            return "";

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string GetPath();
        }
    }
}
