﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetPrice
{
    public class ExcelProcessing
    {
        private string _path;

        public ExcelProcessing()
        {
            _path = Path.Combine(Environment.CurrentDirectory, "Files\\залог.xlsx" );
        }

        public void Processing()
        {
            using (var package = new ExcelPackage(new FileInfo(_path)))
            {
                foreach (var sheet in package.Workbook.Worksheets)
                {
                    ProcessingSheet(sheet, package);
                }
            }
        }

        private void ProcessingSheet(ExcelWorksheet sheet, ExcelPackage package )
        {
            int row = 1;

            while (IsDataExist(sheet, row))
            {
                if (IsArticleAndAmount(sheet, row))
                {
                    string article = sheet.Cells[row, 1].Value.ToString();
                    string price = GetArticlePrice(article);
                    ExcelHelper.CreateCell(sheet, row, 5, price);
                }

                row++;
            }
            package.Save();
        }

        private bool IsDataExist(ExcelWorksheet sheet, int row)
        {
            bool isDataExist = sheet.Cells[row, 1].Value != null || sheet.Cells[row, 4].Value != null;
            return isDataExist;
        }

        private bool IsArticleAndAmount(ExcelWorksheet sheet, int row)
        {
            bool isArticleAndAmount = sheet.Cells[row, 1].Value != null && sheet.Cells[row, 4].Value != null && int.TryParse(sheet.Cells[row, 4].Value.ToString(), out int result);
            return isArticleAndAmount;
        }

        private string GetArticlePrice(string article)
        {
            var carPartPrice = new PriceHelper();
            string price = carPartPrice.GetPriceAutodoc(article);

            if (price == null)
            {
                price = carPartPrice.GetPriceAutoPiter(article);
            }

            return price;
        }

    }

}
