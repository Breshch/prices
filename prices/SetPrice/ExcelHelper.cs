﻿using Microsoft.Office.Interop.Excel;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Excel.Application;
using ExcelPackage = OfficeOpenXml.ExcelPackage;
using ExcelWorksheet = OfficeOpenXml.ExcelWorksheet;

namespace SetPrice
{
    class ExcelHelper
    {
        public static double PixelsToInches(double pixels)
        {
            return (pixels - 7) / 7d + 1;
        }

        public static void CreateCell(ExcelWorksheet sheet, int fromRow, int fromColumn, int toRow, int toColumn, string value, Color? color = null,
            float size = 11, bool isFontBold = false,
            ExcelHorizontalAlignment alignment = ExcelHorizontalAlignment.Center,
            ExcelBorderStyle borderStyle = ExcelBorderStyle.Medium)
        {
            var cell = sheet.Cells[fromRow, fromColumn, toRow, toColumn];
            cell.Merge = true;
            cell.Style.Font.Size = size;
            cell.Style.Font.Bold = isFontBold;
            cell.Style.HorizontalAlignment = alignment;
            cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            cell.Style.Border.BorderAround(borderStyle);
            cell.Style.WrapText = true;

            if (color != null)
            {
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(color.Value);
            }

            cell.Style.Font.Name = "Courier New";
            cell.Value = value;
        }

        public static void CreateCell(ExcelWorksheet sheet, int fromRow, int fromColumn, int toRow, int toColumn, double value, Color? color = null,
            float size = 11, bool isFontBold = false,
            ExcelHorizontalAlignment alignment = ExcelHorizontalAlignment.Center,
            ExcelBorderStyle borderStyle = ExcelBorderStyle.Medium, bool isFractionalPart = false)

        {
            var cell = sheet.Cells[fromRow, fromColumn, toRow, toColumn];
            cell.Merge = true;
            cell.Style.Font.Size = size;
            cell.Style.Font.Bold = isFontBold;
            cell.Style.HorizontalAlignment = alignment;
            cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            cell.Style.Border.BorderAround(borderStyle);
            cell.Style.WrapText = true;
            cell.Style.Numberformat.Format = value % 1 == 0
                ? isFractionalPart
                    ? "#,##0.00"
                    : "#,##0"
                : "#,##0.00";
            if (color != null)
            {
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(color.Value);
            }
            cell.Style.Font.Name = "Courier New";
            cell.Value = value;
        }

        public static void CreateCell(ExcelWorksheet sheet, int row, int column, string value, Color? color = null, float size = 11, bool isFontBold = false,
            ExcelHorizontalAlignment alignment = ExcelHorizontalAlignment.Center,
            ExcelBorderStyle borderStyle = ExcelBorderStyle.Medium)
        {
            CreateCell(sheet, row, column, row, column, value, color, size, isFontBold, alignment, borderStyle);
        }

        public static void CreateCell(ExcelWorksheet sheet, int row, int column, double value, Color? color = null, float size = 11, bool isFontBold = false,
            ExcelHorizontalAlignment alignment = ExcelHorizontalAlignment.Center,
            ExcelBorderStyle borderStyle = ExcelBorderStyle.Medium, bool isFractionalPart = false)
        {
            CreateCell(sheet, row, column, row, column, value, color, size, isFontBold, alignment, borderStyle, isFractionalPart);
        }
    }
}
