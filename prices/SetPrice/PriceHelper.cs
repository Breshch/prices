﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SetPrice
{
    public class PriceHelper
    {
        public string GetPriceAutodoc(string article)
        {

            string baseUrl1 = "https://webapi.autodoc.ru/api/manufacturers/";
            string baseUrl2 = "https://webapi.autodoc.ru/api/spareparts/";
            string baseUrl3 = "/null?isrecross=true";
            string id = "";
            string price;
            string html;

            string url = baseUrl1 + article.Replace(".", "");
            try
            {
                var httpClient = new HttpClient();
                html = httpClient.GetStringAsync(url).Result;
            }
            catch
            {
                return "0";
            }

            var ids = html.Split(',').ToArray();
            id = ids[0].ToString();
            id = id.Substring(id.IndexOf(':', 0) + 2);
            var isNumeric = int.TryParse(id, out int n);

            if (isNumeric)
            {
                
                string newArticle = article.Insert(0, "/");
                url = baseUrl2 + id + newArticle + baseUrl3;
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString(url);
                    var array = json.Split(',').ToArray();
                    int start = array[6].IndexOf(':');
                    price = array[6].Substring(start + 2).Replace('.', ',');

                    if (price == "[]")
                    {
                        GetPriceAutoPiter(article);
                    }
                    Debug.WriteLine(article + " : " + price);
                }

                return price;
            }

            else
            {
                GetPriceAutoPiter(article);
                return "0";
            }
        }

        public string GetPriceAutoPiter(string article)
        {
            string html;
            string baseUrl = "http://autopiter.ru/";

            string url = baseUrl;// + article.Replace(".", "");
            try
            {
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString(url);
                }
                    //var httpClient = new HttpClient();
                    //html = httpClient.GetStringAsync(url).Result;
                }
            catch (Exception ex)
            {
                return "0";
            }
            return "";
        }
    }

   
}
