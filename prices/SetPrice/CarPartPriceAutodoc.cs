﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SetPrice
{
    public class CarPartPriceAutodoc
    {
        private string _baseUrl1 = "https://webapi.autodoc.ru/api/manufacturers/";
        private string _baseUrl2 = "https://webapi.autodoc.ru/api/spareparts/";
        private string _baseUrl3 = "/null?isrecross=true";
        private string _id = "";

        public string GetPrice(string article)
        {
            string price;
            string html;

            string url = _baseUrl1 + article.Replace(".", "");
            try
            {
                var httpClient = new HttpClient();
                html = httpClient.GetStringAsync(url).Result;
            }
            catch
            {
                return "0";
            }
            var ids = html.Split(',').ToArray();
            var id = ids[0].ToString();
            id = id.Substring(id.IndexOf(':', 0) + 2);
            var isNumeric = int.TryParse(id, out int n);

            if (isNumeric && id != null)
            {
                _id = id;
                string newArticle = article.Insert(0, "/");
                url = _baseUrl2 + _id + newArticle + _baseUrl3;
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString(url);
                    var array = json.Split(',').ToArray();
                    int start = array[6].IndexOf(':');
                    price = array[6].Substring(start + 2).Replace('.', ',');
                    Debug.WriteLine(article + " : " + price);
                }

                return price;
            }

            else
            {
                var carPartAutoPiter = new CarPartPriceAutoPiter();
                var carPartPrice = carPartAutoPiter.GetPrice2(article);
                return "0";
            }
        }
    }

    public class CarPartPriceAutoPiter
    {
        private string _baseUrl = "https://autopiter.ru/goods/";
        public string GetPrice2(string article)
        {
            string html;

            string url = _baseUrl + article.Replace(".", "");
            try
            {
                var httpClient = new HttpClient();
                html = httpClient.GetStringAsync(url).Result;
            }
            catch
            {
                return "0";
            }
            return "";
        }
    }
}
